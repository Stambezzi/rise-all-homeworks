﻿namespace Day4Tasks
{
    public class FindRootOfThree
    {
        public static int GetRootOfThree(int number) 
        {
            if (number == 0)
            {
                return 0;
            }

            for (var i = 1; i <= number; i++)
            {
                if (i * i * i == number)
                {
                    return i;
                }
            }

            throw new ArgumentException("The cube root could not be found.");
        }
    }
}

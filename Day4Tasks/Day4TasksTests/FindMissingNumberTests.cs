using Day4Tasks;

namespace Day4TasksTests
{
    [TestClass]
    public class FindMissingNumberTests
    {
        [TestMethod]
        public void TestGetMissingNumberShouldReturnMissingNumberWithUnsortedElements()
        {
            var numbers = new int[] { 8, 1, 4, 6, 2, 7, 3, 10, 9 };

            var expected = 5;
            var actual = FindMissingNumber.GetMissingNumber(numbers);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestGetMissingNumberShouldReturnMissingNumberWithSortedElements()
        {
            var numbers = new int[] { 7, 8, 9, 10, 11, 13, 14, 15 };

            var expected = 12;
            var actual = FindMissingNumber.GetMissingNumber(numbers);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestGetMissingNumberShouldReturnMissingNumberWithTwoUnsortedElements()
        {
            var numbers = new int[] { 19, 17 };

            var expected = 18;
            var actual = FindMissingNumber.GetMissingNumber(numbers);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestGetMissingNumberShouldReturnMissingNumberWithTwoSortedElements()
        {
            var numbers = new int[] { 18, 20 };

            var expected = 19;
            var actual = FindMissingNumber.GetMissingNumber(numbers);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestGetMissingNumberShouldThrowForArrayWithOneElement()
        {
            var numbers = new int[] { 18 };

            var exceptionMessage = "The array must contain at least 2 elements";
            Assert.ThrowsException<ArgumentException>(() => FindMissingNumber.GetMissingNumber(numbers), exceptionMessage);
        }

        [TestMethod]
        public void TestGetMissingNumberShouldThrowForEmptyArray()
        {
            var numbers = new int[] {};

            var exceptionMessage = "The array must contain at least 2 elements";
            Assert.ThrowsException<ArgumentException>(() => FindMissingNumber.GetMissingNumber(numbers), exceptionMessage);
        }

        [TestMethod]
        public void TestGetMissingNumberShouldThrowForNotFoundElement()
        {
            var numbers = new int[] { 7, 5, 6};

            var exceptionMessage = "No missing number found in the array.";
            Assert.ThrowsException<ArgumentException>(() => FindMissingNumber.GetMissingNumber(numbers), exceptionMessage);
        }
    }
}
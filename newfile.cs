using System;

class NewFile
{
    public static void Main(string[] args)
    {

        // printing Hello World!
        Console.WriteLine("Hello World!");
        // To prevents the screen from
        // running and closing quickly
        Console.ReadKey();
    }
}
﻿namespace Day7TasksTests
{
    using Day7Tasks;

    [TestClass]
    public class FifthTaskTests
    {
        [TestMethod]
        public void GetOrderedElementsShouldWorkProperly() 
        {
            int[,] matrix = new int[,]
            {
                {1, 2, 3, 4},
                {6, 10, 11, 12},
                {5, 7, 8, 9}
            };

            var expected = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
            var actual = FifthTask.GetOrderedElements(matrix);

            CollectionAssert.AreEqual(actual, expected);
        }
    }
}

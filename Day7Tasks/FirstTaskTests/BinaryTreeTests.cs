namespace FirstTaskTests
{
    using Day7Tasks;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class BinaryTreeTests
    {
        private BinaryTree<int> node24;
        private BinaryTree<int> node11;
        private BinaryTree<int> node35;
        private BinaryTree<int> node61;
        private BinaryTree<int> node76;
        private BinaryTree<int> node38;
        private BinaryTree<int> node69;
        private BinaryTree<int> node52;
        private BinaryTree<int> root;

        [TestInitialize]
        public void SetUp()
        {
            node24 = new BinaryTree<int>(24, null, null);
            node11 = new BinaryTree<int>(11, null, node24);

            node35 = new BinaryTree<int>(35, null, null);
            node61 = new BinaryTree<int>(61, null, null);
            node76 = new BinaryTree<int>(76, null, null);

            node38 = new BinaryTree<int>(38, node35, null);
            node69 = new BinaryTree<int>(69, node61, node76);

            node52 = new BinaryTree<int>(52, node38, node69);
            root = new BinaryTree<int>(25, node11, node52);
        }

        [TestMethod]
        public void BinaryTreePreOrderShouldWorkProperly()
        {
            var expected = new List<int> { 25, 11, 24, 52, 38, 35, 69, 61, 76 };
            var actual = root.PreOrder();

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void BinaryTreePostOrderShouldWorkProperly()
        {
            var expected = new List<int> { 24, 11, 35, 38, 61, 76, 69, 52, 25};
            var actual = root.PostOrder();

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void BinaryTreeInOrderShouldWorkProperly()
        {
            var expected = new List<int> { 11, 24, 25, 35, 38, 52, 61, 69, 76 };
            var actual = root.InOrder();

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsBinarySearchTreeShouldSucceed() 
        {
            var result = root.IsBinarySearchTree();

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsBinarySearchTreeShouldFailForNonBinarySearchTree()
        {
            var node11 = new BinaryTree<int>(11, null, null);
            var node33 = new BinaryTree<int>(33, null, null);

            var root = new BinaryTree<int>(50, node33, node11);

            var result = root.IsBinarySearchTree();

            Assert.IsFalse(result);

            var list = new List<int>();         
        }

        [TestMethod]
        public void GetKthSmallestElementShouldWorkProperly()
        {
            var expected = 25;
            var actual = root.GetKthSmallestElement(3);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetKthSmallestElementThrowsForNotExistingPosition()
        {
            var exceptionMessage = "No element at that position";

            Assert.ThrowsException<ArgumentException>(() => root.GetKthSmallestElement(20), exceptionMessage);
        }

        [TestMethod]
        public void GetTreeHeightShouldWorkProperly() 
        {
            var expected = 4;
            var actual = BinaryTree<int>.GetTreeHeight(root);

            Assert.AreEqual(expected, actual);
        }
    }
}
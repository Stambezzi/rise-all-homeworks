﻿namespace Day7Tasks
{
    public class SixthTask
    {
        // 6. Implement Heap Sort
        public static int[] HeapSort(int[] unsortedNumbers) 
        {
            if (unsortedNumbers.Length == 0)
            {
                throw new ArgumentException("The length of the array should be at least 1");
            }

            var elements = new PriorityQueue<int, int>();

            foreach (var number in unsortedNumbers)
            {
                elements.Enqueue(number, number);
            }

            var sortedNumbers = new List<int>();

            while (elements.Count > 0)
            {
                sortedNumbers.Add(elements.Dequeue());
            }

            return sortedNumbers.ToArray();
        }
    }
}

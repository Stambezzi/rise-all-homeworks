﻿namespace SmallTasks
{
    public class TasksWithNumbers
    {

        // 2. Odd number? bool IsOdd(int n)

        public static bool IsOdd(int n)
        {
            return n % 2 != 0;
        }

        // 3. Prime number? bool IsPrime(int N)

        public static bool IsPrime(int n)
        {
            var counter = 0;

            for (int i = 1; i <= n; i++)
            {
                if (n % i == 0)
                {
                    counter++;
                }
            }
            if (counter == 2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // 4. Min element inside an array? int Min(int... array)

        public static int Min(int[] numbers)
        {
            var minValue = int.MaxValue;

            for (int i = 0; i < numbers.Length; i++)
            {
                var currentNumber = numbers[i];

                if (currentNumber < minValue)
                {
                    minValue = currentNumber;
                }
            }

            return minValue;
        }

        // 5. k-th min element inside an array? int KthMin(int k, int[] array)

        public static int KthMin(int k, int[] numbers)
        {
            Array.Sort(numbers);

            return numbers[k];
        }

        // 6. Find a number, that occurs odd times in an array int GetOddOccurrence(int... array)

        public static int GetOddOccurrence(int[] numbers)
        {
            var numbersCountOccurrrences = new Dictionary<int, int>();

            foreach (var number in numbers)
            {
                if (numbersCountOccurrrences.ContainsKey(number))
                {
                    numbersCountOccurrrences[number]++;
                }
                else
                {
                    numbersCountOccurrrences[number] = 1;
                }
            }

            foreach (var (number, occurrences) in numbersCountOccurrrences)
            {
                if (occurrences % 2 != 0)
                {
                    return number;
                }
            }

            return -1;
        }

        // 7. Average of elements inside that array? int GetAverage(int[] array);

        public static int GetAverage(int[] array)
        {
            var sum = 0;

            for (int i = 0; i < array.Length; i++)
            {
                sum += array[i];
            }

            var result = sum / array.Length;

            return result;
        }

        // 8. Raise an integer to a power of another (Write a O(log(b)) solution) long pow(int a, int b)

        public static long Pow(int a, int b) // but with O(log N) 
        {
            long result = 1;

            for (int i = 1; i <= b; i++)
            {
                result *= a;
            }

            return result;
        }

        // 9. Smallest multiple of a number - a number that can be divided by each of the numbers from one to a given N long GetSmallestMultiple(int N);

        public static long GetSmallestMultiple(int N)
        {
            var factorial = Factorial(N);

            for (long i = 1; i <= factorial; i++)
            {
                for (long j = 1; j <= N; j++)
                {
                    if (i % j != 0)
                    {
                        break;
                    }
                    if (j == N)
                    {
                        return i;
                    }
                }
            }
            return 0;
        }

        private static long Factorial(int N)
        {
            long result = 1;

            for (long i = 1; i <= N; i++)
            {
                result *= i;
            }

            return result;
        }

        // 10. Double factorial? long DoubleFac(int n); if n=3, => (3!)! = 6! = 720

        public static long DoubleFac(int n)
        {
            if (n == 0)
            {
                return 1;
            }
            else if (n == 2)
            {
                return 2;
            }

            var result = 1L;
            var count = 2 * n;

            for (int i = 1; i <= count; i++)
            {
                result *= i;
            }

            return result;
        }

        // 11. k-th factorial? long KthFac(int k, int n);

        public static long KthFac(int k, int n) 
        {
            if (n == 0)
            {
                return 1;
            }
            else if (n == 2)
            {
                return 2;
            }
            
            var result = 1L;
            var count = k * n;

            for (int i = 1; i <= count; i++)
            {
                result *= i;
            }

            return result;
        }

        // 12. maximal scalar product - by two vectors given a and b, return their scalar product (as a bonus, find a permutation of a and b where their scalar product is max) long MaximalScalarSum(int[] a, int[] b)

        public static int MaximalScalarSum(int[] a, int[] b)
        {
            if (a.Length != b.Length)
            {
                throw new ArgumentException();
            }

            var sum = 0;

            Array.Sort(a);
            Array.Sort(b, (x, y) => -x.CompareTo(y));

            for (int i = 0; i < a.Length; i++)
            {
                sum += a[i] * b[i];
            }

            return sum;
        }

        // 13. max span - return the max number of elements (span) between a element from the left and from the right side of an array that is equal. A single number has a span of 1. int MaxSpan(int[] numbers) maxSpan(2, 5, 4, 1, 3, 4) → 4 MaxSpan(8, 12, 7, 1, 7, 2, 12) → 6 MaxSpan(3, 6, 6, 8, 4, 3, 6) → 6

        public static int MaxSpan(int[] numbers) 
        {
            var maxDifference = 0;

            for (int i = 0; i < numbers.Length; i++)
            {
                var currentNumber = numbers[i];

                for (int j = numbers.Length - 1; j >= 0; j--)
                {
                    if (currentNumber == numbers[j])
                    {
                        var difference = j - i + 1;

                        if (difference > maxDifference)
                        {
                            maxDifference = difference;
                        }

                        break;
                    }
                }
            }

            return maxDifference;
        }

        // equal sum sides - return true if there is an element in the array, that you can remove and those from the left and right have the same sum bool EqualSumSides(int[] numbers) EqualSumSides(3, 0, -1, 2, 1) → true EqualSumSides(2, 1, 2, 3, 1, 4) → true EqualSumSides(8, 8) → false

        public static bool EqualSumSides(int[] numbers) 
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                var leftSum = 0;
                var rightSum = 0;

                for (int j = 0; j < i; j++)
                {
                    leftSum += numbers[j];
                }

                for (int j = i + 1; j < numbers.Length; j++)
                {
                    rightSum += numbers[j];
                }

                if (leftSum == rightSum)
                {
                    return true;
                }
            }

            return false;
        }
    }
}

﻿using Day5Tasks;

namespace Day5TasksTests
{
    [TestClass]
    public class FourthTaskTests
    {
        [TestMethod]
        public void GetExpandedExpressionShouldSucceed() 
        {
            var expression = "AB3(DC)2(F)2(E3(G))";

            var expected = "ABDCDCDCFFEGGGEGGG";
            var actual = FourthTask.GetExpandedExpression(expression);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetExpandedExpressionShouldSucceedWithSimpleEpression()
        {
            var expression = "ABC";

            var expected = "ABC";
            var actual = FourthTask.GetExpandedExpression(expression);

            Assert.AreEqual(expected, actual);
        }
    }
}

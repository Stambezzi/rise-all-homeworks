﻿namespace Day5Tasks
{
    public class SecondTask
    {
        // Erase the middle element of a given Linked list

        public static void EraseMiddleElement<T>(LinkedList<T> elements) 
        {
            if (elements.Count() < 3)
            {
                throw new ArgumentException("List should contain at least 3 elements");
            }

            var count = elements.Count / 2;

            var elementToDelete = elements.First;

            for (int i = 0; i < count; i++)
            {
                elementToDelete = elementToDelete.Next;
            }

            elements.Remove(elementToDelete);
        }
    }
}

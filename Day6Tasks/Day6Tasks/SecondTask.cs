﻿namespace Day6Tasks
{
    using System;
    using System.Collections.Generic;

    public class SecondTask
    {
        /* Task 2: Find the intersection of two arrays. In other words, find all the elements that are present 
           in both arrays and return them as array. */

        public static string[] GetIntersectionOfArrays(string[] firstArray, string[] secondArray)
        {
            // firstArray.Union(secondArray); - Build in function

            if (firstArray.Count() == 0 || secondArray.Count() == 0)
            {
                throw new ArgumentException("Arrays should contain at least 1 element");
            }

            // var-ове, mar-ове
            var dictionary = new Dictionary<string, int>();

            // var
            foreach (var element in firstArray)
            {
                dictionary.Add(element, 1);
            }

            // var
            foreach (var element in secondArray)
            {
                if (dictionary.ContainsKey(element))
                {
                    dictionary[element]++;
                }
            }

            var result = new List<string>();

            foreach (var (element, count) in dictionary.Where(kvp => kvp.Value > 1)) 
            {
                result.Add(element);
            }

            // Супер е, отново хубави тестове
            return result.ToArray();
        }
    }
}

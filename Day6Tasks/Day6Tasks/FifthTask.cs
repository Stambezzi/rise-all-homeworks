﻿namespace Day6Tasks
{
    public class FifthTask
    {
        /*Task 5: Given a list of words, group them by anagrams. Anagrams are words that have the same 
         letters in a different order. */

        public static Dictionary<string, List<string>> GroupWordsByAnagrams(List<string> words)
        {
            // Избягвай да ползваш var - това прави кода по-труден за четене
            var anagrams = new Dictionary<string, SortedDictionary<char, int>>();
            var resultDictionary = new Dictionary<string, List<string>>();

            foreach (var word in words)
            {
                if (!anagrams.ContainsKey(word))
                {
                    anagrams.Add(word, new SortedDictionary<char, int>());
                    resultDictionary.Add(word, new List<string>());

                    foreach (var c in word)
                    {
                        if (!anagrams[word].ContainsKey(c))
                        {
                            anagrams[word].Add(c, 0);
                        }

                        anagrams[word][c]++;
                    }
                }
            }

            // Тук например е по-ок да ползваш var, според мен, защото е някакъв грозен тип данни
            foreach (var (currentWord, dict) in anagrams)
            {
                foreach (var (word, dict2) in anagrams)
                {
                    if (currentWord != word && dict.SequenceEqual(dict2))
                    {
                        resultDictionary[currentWord].Add(word);
                    }
                }
            }

            return resultDictionary;
        }
    }
}

// Отново имаш хубави тестове, браво!
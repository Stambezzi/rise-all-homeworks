﻿namespace Day6Tasks
{
    public class ThirdTask
    {
        /*Given a string, find the:
         a. All non-repeating characters and return them in any suitable structure.
         b. First non-repeating character in it and return its index. If it doesn't exist, return -
         1*/

        public static List<char> GetAllNonRepeatingChars(string word) 
        {
            // var, var, var
            var dictionary = new Dictionary<char, int>();

            foreach (char c in word) 
            {
                if (!dictionary.ContainsKey(c))
                {
                    dictionary.Add(c, 0);
                }

                dictionary[c]++;
            }

            var list = new List<char>();

            foreach (var (ch, occurrence) in dictionary)
            {
                if (occurrence == 1)
                {
                    list.Add(ch);
                }
            }

            return list;
        }

        public static int GetIndexOfFirstNonRepeatingChar(string word) 
        {
            var nonrepeatingChars = GetAllNonRepeatingChars(word);

            for (int i = 0; i < word.Length; i++)
            {
                if (nonrepeatingChars.Contains(word[i]))
                {
                    // Избягвай мултипъл ритърни, не са добра практика
                    return i;
                }
            }

            return -1;
        }
    }
}

// Много добре си се справила с всички задача, само някакви дребни детайли, освновно var-овете. 
// Страхотни тестове, което си е важно. 
// пробвай да пускаш MR-и следващите пъти. 

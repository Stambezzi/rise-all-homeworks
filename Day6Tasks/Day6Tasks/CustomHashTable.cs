﻿namespace Day6Tasks
{
    using System;
    using System.Collections.Generic;

    // Имплементирала си HashSet, за да е HashTable елементите трябва да са някакви двойки
    // Също така може да пробваш да ползваш Generics, за да може да я ползваш за всякакви типове, а не само стринг
    // Но това е по-трудно
    public class CustomHashTable
    {
        private LinkedList<string>[] elements;
        private int count;

        public CustomHashTable()
        {
            this.elements = new LinkedList<string>[10];
            for (int i = 0; i < this.elements.Length; i++)
            {
                this.elements[i] = new LinkedList<string>();
            }
        }

        public int GetCount()
        {
            return this.count;
        }

        public void Insert(string element)
        {
            // Избягвай да ползваш var на всякъде, особено когато е прост тип
            var indexToInsert = this.GetArrayPosition(element);

            this.elements[indexToInsert].AddFirst(element);
            this.count++;
        }

        public bool Contains(string element)
        {
            // Избягвай да ползваш var на всякъде, особено когато е прост тип
            var index = this.GetArrayPosition(element);

            return this.elements[index].Contains(element);
        }

        public void Delete(string element)
        {
            if (!this.Contains(element))
            {
                throw new ArgumentException("The element does not exist");
            }

            // Избягвай да ползваш var на всякъде, особено когато е прост тип
            var index = this.GetArrayPosition(element);

            this.elements[index].Remove(element);
            this.count--;
        }

        private int GetArrayPosition(string element)
        {
            // Избягвай да ползваш var на всякъде, особено когато е прост тип
            var position = 0;

            for (int i = 0; i < element.Length; i++)
            {
                position += element[i];
            }

            // функцията за хеширане е добре, но не виждам resize() функция. Т.е. в случай че елементите станат много това ще е почти масив.
            return position % 10;
        }
    }

    // Не виждам функция за достъпване на елемент
}

// Като цяло си направила по-голямата част от задачата, може би не ти е останло време за послендите 2-3 неща
// или просто ти е писнало?
// Имаш добри тестове!
﻿namespace Day6TasksTests
{
    using Day6Tasks;

    [TestClass]
    public class PersonTests
    {
        [TestMethod]
        public void PersonShouldAssumeTwoObjectsAreEqual() 
        {
            var firstPerson = new Person("Petya", "Minkova", 15);
            var secondPerson = new Person("Petya", "Minkova", 15);

            var hashSet = new HashSet<Person>();

            hashSet.Add(firstPerson);
            hashSet.Add(secondPerson);

            Assert.AreEqual(1, hashSet.Count);
        }

        public void PersonShouldAssumeTwoObjectsWithSameNamesAreNotEqual()
        {
            var firstPerson = new Person("Petya", "Minkova", 15);
            var secondPerson = new Person("Petya", "Minkova", 20);

            var hashSet = new HashSet<Person>();

            hashSet.Add(firstPerson);
            hashSet.Add(secondPerson);

            Assert.AreEqual(1, hashSet.Count);
        }
    }
}

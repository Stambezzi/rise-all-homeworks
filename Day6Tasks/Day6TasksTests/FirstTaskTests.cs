namespace Day6TasksTests
{
    using Day6Tasks;

    [TestClass]
    public class FirstTaskTests
    {
        [TestMethod]
        public void GetCoolestNumberOwnerShouldReturnOwnersName()
        {
            var carsNumbers = new Dictionary<string, string>();

            carsNumbers.Add("CA1234BG", "Ivan Ivanov");
            carsNumbers.Add("CA5672RS", "Nikola Dimitrov");
            carsNumbers.Add("CA1234VH", "Stoyan Ivanov");
            carsNumbers.Add("CA6789BG", "Petya Ivanova");
            carsNumbers.Add("XAXAXAXA", "Damyan Grozdev");
            carsNumbers.Add("CA1284RS", "Penio Penev");
            carsNumbers.Add("CA9876BG", "Penio Penev");
            carsNumbers.Add("CA5555BG", "Petya Ivanova");
            carsNumbers.Add("CA4567BG", "Teodor Kostov");
            carsNumbers.Add("CA1111BG", "Nadya Todorova");

            var expected = "Damyan Grozdev";
            var actual = FirstTask.GetCoolestNumberOwner(carsNumbers);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetCoolestNumberOwnerThrowsWhenCoolestNumberIsMissing()
        {
            var carsNumbers = new Dictionary<string, string>();

            carsNumbers.Add("CA1234BG", "Ivan Ivanov");
            carsNumbers.Add("CA5672RS", "Nikola Dimitrov");
            carsNumbers.Add("CA1234VH", "Stoyan Ivanov");
            carsNumbers.Add("CA6789BG", "Petya Ivanova");
            carsNumbers.Add("CA1284RS", "Penio Penev");
            carsNumbers.Add("CA9876BG", "Penio Penev");
            carsNumbers.Add("CA5555BG", "Petya Ivanova");
            carsNumbers.Add("CA4567BG", "Teodor Kostov");
            carsNumbers.Add("CA1111BG", "Nadya Todorova");

            var exceptionMessage = "The coolest number is not present in the list";

            Assert.ThrowsException<ArgumentException>(() => FirstTask.GetCoolestNumberOwner(carsNumbers), exceptionMessage);
        }

        [TestMethod]
        public void GetOwnersWithMoreThenOneCarShouldReturnListWithOwners() 
        {
            var carsNumbers = new Dictionary<string, string>();

            carsNumbers.Add("CA1234BG", "Ivan Ivanov");
            carsNumbers.Add("CA5672RS", "Nikola Dimitrov");
            carsNumbers.Add("CA1234VH", "Stoyan Ivanov");
            carsNumbers.Add("CA6789BG", "Petya Ivanova");
            carsNumbers.Add("CA1284RS", "Penio Penev");
            carsNumbers.Add("CA9876BG", "Penio Penev");
            carsNumbers.Add("CA5555BG", "Petya Ivanova");
            carsNumbers.Add("CA4567BG", "Teodor Kostov");
            carsNumbers.Add("CA1111BG", "Nadya Todorova");

            var expected = new List<string>() { "Petya Ivanova", "Penio Penev" };
            var actual = FirstTask.GetOwnersWithMoreThenOneCar(carsNumbers);

            CollectionAssert.AreEquivalent(expected, actual);
        }


        [TestMethod]
        public void GetOwnersWithMoreThenOneCarShouldReturnEmptyCollection()
        {
            var carsNumbers = new Dictionary<string, string>();

            carsNumbers.Add("CA1234BG", "Ivan Ivanov");
            carsNumbers.Add("CA5672RS", "Nikola Dimitrov");
            carsNumbers.Add("CA1234VH", "Stoyan Ivanov");
            carsNumbers.Add("CA6789BG", "Petya Ivanova");
            carsNumbers.Add("CA9876BG", "Penio Penev");
            carsNumbers.Add("CA4567BG", "Teodor Kostov");
            carsNumbers.Add("CA1111BG", "Nadya Todorova");

            var expected = new List<string>() {};
            var actual = FirstTask.GetOwnersWithMoreThenOneCar(carsNumbers);

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
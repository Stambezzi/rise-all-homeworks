﻿namespace Day6TasksTests
{
    using Day6Tasks;

    [TestClass]
    public class FourthTaskTests
    {
        [TestMethod]
        public void GetAllMisspelledWordsShouldWorkProperly() 
        {
            var words = new HashSet<string>();

            words.Add("Today");
            words.Add("I");          
            words.Add("feel");
            words.Add("great");

            var document = "Today i feel greet";

            var expected = new List<string>() {"i", "greet"};
            var actual = FourthTask.GetAllMisspelledWords(words, document);

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetAllMisspelledWordsReturnsEmptyCollectionForNonMisspelledWords()
        {
            var words = new HashSet<string>();

            words.Add("Today");
            words.Add("I");
            words.Add("feel");
            words.Add("great");

            var document = "Today I feel great";

            var expected = new List<string>() {};
            var actual = FourthTask.GetAllMisspelledWords(words, document);

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}

﻿namespace Day6TasksTests
{
    using Day6Tasks;

    using System;

    [TestClass]
    public class SecondTaskTests
    {
        [TestMethod]
        public void GetIntersectionOfArraysShouldWorkProperly() 
        {
            var firstArray = new string[] { "Monday", "Tuesday", "Wednesday", "Friday", "Sunday"};
            var secondArray = new string[] { "Friday", "Saturday", "Sunday" };

            var expected = new string[] { "Friday", "Sunday" };
            var actual = SecondTask.GetIntersectionOfArrays(firstArray, secondArray);

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetIntersectionOfArraysShouldReturnEmptyArrayForNonIntersection()
        {
            var firstArray = new string[] { "Monday", "Tuesday", "Wednesday"};
            var secondArray = new string[] { "Friday", "Saturday", "Sunday" };

            var expected = new string[] {};
            var actual = SecondTask.GetIntersectionOfArrays(firstArray, secondArray);

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetIntersectionOfArraysThrowsForEmptyArray()
        {
            var firstArray = new string[] {};
            var secondArray = new string[] { "Friday", "Saturday", "Sunday" };

            var exceptionMessage = "Arrays should contain at least 1 element";

            Assert.ThrowsException<ArgumentException>(()
                => SecondTask.GetIntersectionOfArrays(firstArray, secondArray), exceptionMessage);
        }
    }
}

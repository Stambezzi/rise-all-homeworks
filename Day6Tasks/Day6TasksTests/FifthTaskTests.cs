﻿namespace Day6TasksTests
{
    using Day6Tasks;

    [TestClass]
    public class FifthTaskTests
    {
        [TestMethod]
        public void GroupWordsByAnagramsShouldReturnCorrectResult() 
        {
            var words = new List<string>() { "banana", "ananab", "abanan", "stone" };

            var expexted = new Dictionary<string, List<string>>();

            expexted.Add("banana", new List<string> { "ananab", "abanan" });
            expexted.Add("ananab", new List<string> { "banana", "abanan" });
            expexted.Add("abanan", new List<string> { "banana", "ananab" });
            expexted.Add("stone", new List<string>());

            var actual = FifthTask.GroupWordsByAnagrams(words);

            var expectedString = ToAssertableString(expexted);
            var actualString = ToAssertableString(actual);

            Assert.AreEqual(expectedString, actualString);
        }

        private string ToAssertableString(Dictionary<string, List<string>> dictionary)
        {
            var pairStrings = dictionary.Select(p => p.Key + ": " + string.Join(", ", p.Value));
            return string.Join("; ", pairStrings);
        }
    }
}

﻿namespace Day6TasksTests
{
    using Day6Tasks;

    [TestClass]
    public class ThirdTaskTests
    {
        [TestMethod]
        public void GetAllNonRepeatingCharsShouldWorkProperlyWithNonRepeatingChars() 
        {
            var word = "banana";

            var expected = new List<char>() {'b'};
            var actual = ThirdTask.GetAllNonRepeatingChars(word);

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetAllNonRepeatingCharsShouldReturnEmptyCollectionWithRepeatingChars()
        {
            var word = "mama";

            var expected = 0;
            var actual = ThirdTask.GetAllNonRepeatingChars(word).Count;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetIndexOfFirstNonRepeatingCharShouldWorkProperly() 
        {
            var word = "banana";

            var expected = 0;
            var actual = ThirdTask.GetIndexOfFirstNonRepeatingChar(word);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetIndexOfFirstNonRepeatingCharShouldReturnMinusOneForMissingNonRepeatingChars()
        {
            var word = "mama";

            var expected = -1;
            var actual = ThirdTask.GetIndexOfFirstNonRepeatingChar(word);

            Assert.AreEqual(expected, actual);
        }
    }
}
